# ov7670-driver

A simple driver for the OV7670 for the STM32 platform. This repo is used as a sub module for the main STM32 base project.
Find the main project by going back into the root of the group.

# Contributing

Currently we don't have any guides of anything for new contributors, just add or fix something then create a PR and I will take a look.
